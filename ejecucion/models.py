from django.db import models

class UserStory(models.Model):
    nombre:models.CharField(max_length=20, blank=True, null=False)
    #tipoUS=
    valor_tecnico=models.IntegerField()
    valor_negocio=models.IntegerField()
    prioridad=models.DecimalField()
    descripcion_breve=models.CharField(max_length=100, blank=True, null=False)
    tiempo_estimado=models.IntegerField(help_text=El valor ingresado serà en horas)
    #sprint=
    criterio_aceptacion=models.DecimalField()
    usuario_asignado=models.ForeignKey(get_)